package nau.launcher.launchernau2.pojo;

import nau.launcher.launchernau2.utils.UserHandle;

public class AppPojo extends PojoWithTags {
    public String packageName;
    public String activityName;
    public UserHandle userHandle;

    public String getComponentName() {
        return userHandle.addUserSuffixToString(packageName + "/" + activityName, '#');
    }
}
