package nau.launcher.launchernau2.db;

public class ValuedHistoryRecord {
    /**
     * ID for the record
     */
    public String record;

    /**
     * Context dependant value, e.g. number of access
     */
    public int value;
}
