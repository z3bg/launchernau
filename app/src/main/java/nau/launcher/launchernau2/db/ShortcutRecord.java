package nau.launcher.launchernau2.db;

public class ShortcutRecord {

    public String name;

    public String packageName;

    public String iconResource;

    public String intentUri;

    public byte[] icon_blob;

}
