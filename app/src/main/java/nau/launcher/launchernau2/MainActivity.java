package nau.launcher.launchernau2;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.DataSetObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.BannerCallbacks;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.MessageDigestSpi;
import java.util.ArrayList;
import java.util.Base64;

import kotlin.jvm.internal.Intrinsics;

import nau.launcher.launchernau2.adapter.RecordAdapter;
import nau.launcher.launchernau2.broadcast.IncomingCallHandler;
import nau.launcher.launchernau2.broadcast.IncomingSmsHandler;
import nau.launcher.launchernau2.forwarder.ForwarderManager;
import nau.launcher.launchernau2.searcher.ApplicationsSearcher;
import nau.launcher.launchernau2.searcher.HistorySearcher;
import nau.launcher.launchernau2.searcher.QueryInterface;
import nau.launcher.launchernau2.searcher.QuerySearcher;
import nau.launcher.launchernau2.searcher.Searcher;
import nau.launcher.launchernau2.searcher.TagsSearcher;
import nau.launcher.launchernau2.ui.AnimatedListView;
import nau.launcher.launchernau2.ui.BottomPullEffectView;
import nau.launcher.launchernau2.ui.KeyboardScrollHider;
import nau.launcher.launchernau2.ui.ListPopup;
import nau.launcher.launchernau2.ui.SearchEditText;
import nau.launcher.launchernau2.utils.PackageManagerUtils;
import nau.launcher.launchernau2.utils.SystemUiVisibilityHelper;
import nau.mynau.Api;
import nau.mynau.AsyncResponse;
import nau.mynau.DownloadImageTask;
import nau.mynau.MainLoginActivity;
import nau.mynau.OnSwipeTouchListener;
import nau.mynau.myNauFeedActivity;

import static android.view.HapticFeedbackConstants.LONG_PRESS;

public class MainActivity extends Activity implements QueryInterface, KeyboardScrollHider.KeyboardHandler, View.OnTouchListener, Searcher.DataObserver, AsyncResponse {

    public static final String START_LOAD = "fr.neamar.summon.START_LOAD";
    public static final String LOAD_OVER = "fr.neamar.summon.LOAD_OVER";
    public static final String FULL_LOAD_OVER = "fr.neamar.summon.FULL_LOAD_OVER";

    private static final String TAG = "MainActivity";

    /**
     * Adapter to display records
     */
    public RecordAdapter adapter;

    /**
     * Store user preferences
     */
    public SharedPreferences prefs;

    /**
     * Receive events from providers
     */
    private BroadcastReceiver mReceiver;

    /**
     * View for the Search text
     */
    public SearchEditText searchEditText;

    /**
     * Main list view
     */
    public AnimatedListView list;
    public View listContainer;
    /**
     * View to display when list is empty
     */
    public View emptyListView;
    /**
     * Utility for automatically hiding the keyboard when scrolling down
     */
    private KeyboardScrollHider hider;
    /**
     * Menu button
     */
    private View menuButton;
    /**
     * Kiss bar
     */
    public View kissBar;
    /**
     * Favorites bar. Can be either the favorites within the LauncherNAU bar,
     * or the external favorites bar (default)
     */
    public View favoritesBar;
    /**
     * Progress bar displayed when loading
     */
    private View loaderSpinner;
    /**
     * Launcher button, can be clicked to display all apps
     */
    private View launcherButton;
    /**
     * "X" button to empty the search field
     */
    private View clearButton;

    /**
     * Task launched on text change
     */
    private Searcher searchTask;

    /**
     * SystemUiVisibility helper
     */
    private SystemUiVisibilityHelper systemUiVisibilityHelper;

    /**
     * myNAU button, to do login on myNAU
     */
    private LinearLayout mynauButton;

    /**
     * Is the LauncherNAU bar currently displayed?
     * (flag updated before animation is over)
     */
    private boolean isDisplayingKissBar = false;

    private PopupWindow mPopup;

    private ForwarderManager forwarderManager;

    private String appKey = "186a4216cf724324508fd0792349d104b8e70803a5f6fe19";

//    private static WeakReference<Activity> sActivity;
    @NotNull private Menu appMenu;
    private MenuItem loginBTN;
    private TextView username;
    private ImageView icon;
    private ImageView avatar;
    private boolean shouldRunOnResumeActions = false;

    private static final int SWIPE_DISTANCE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;
    Intent intentSwipe = new Intent();

    /**
     * Called when the activity is first created.
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");

        KissApplication.getApplication(this).initDataHandler();

        /*
         * Initialize preferences
         */
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        /*
         * Initialize all forwarders
         */
        forwarderManager = new ForwarderManager(this);

        /*
         * Initialize data handler and start loading providers
         */
        IntentFilter intentFilterLoad = new IntentFilter(START_LOAD);
        IntentFilter intentFilterLoadOver = new IntentFilter(LOAD_OVER);
        IntentFilter intentFilterFullLoadOver = new IntentFilter(FULL_LOAD_OVER);
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //noinspection ConstantConditions
                if (intent.getAction().equalsIgnoreCase(LOAD_OVER)) {
                    updateSearchRecords();
                } else if (intent.getAction().equalsIgnoreCase(FULL_LOAD_OVER)) {
                    Log.v(TAG, "All providers are done loading.");

                    displayLoader(false);

                    // Run GC once to free all the garbage accumulated during provider initialization
                    System.gc();
                }

                // New provider might mean new favorites
                onFavoriteChange();
            }
        };

        this.registerReceiver(mReceiver, intentFilterLoad);
        this.registerReceiver(mReceiver, intentFilterLoadOver);
        this.registerReceiver(mReceiver, intentFilterFullLoadOver);

        /*
         * Set the view and store all useful components
         */
        setContentView(R.layout.main);
        this.list = this.findViewById(android.R.id.list);
        this.listContainer = (View) this.list.getParent();
        this.emptyListView = this.findViewById(android.R.id.empty);
        this.kissBar = findViewById(R.id.mainKissbar);
        this.menuButton = findViewById(R.id.menuButton);
        this.searchEditText = findViewById(R.id.searchEditText);
        this.loaderSpinner = findViewById(R.id.loaderBar);
        this.launcherButton = findViewById(R.id.launcherButton);
        this.clearButton = findViewById(R.id.clearButton);
        this.mynauButton = findViewById(R.id.myNauLoginLayout);

        /*
         * Initialize components behavior
         * Note that a lot of behaviors are also initialized through the forwarderManager.onCreate() call.
         */
        displayLoader(true);

        // Add touch listener for history popup to root view
        findViewById(android.R.id.content).setOnTouchListener(this);

        // add history popup touch listener to empty view (prevents on not working there)
        this.emptyListView.setOnTouchListener(this);

        // Create adapter for records
        this.adapter = new RecordAdapter(this, this, new ArrayList<>());
        this.list.setAdapter(this.adapter);

        this.list.setOnItemClickListener((parent, v, position, id) -> adapter.onClick(position, v));

        this.list.setLongClickable(true);
        this.list.setOnItemLongClickListener((parent, v, pos, id) -> {
            ((RecordAdapter) parent.getAdapter()).onLongClick(pos, v);
            return true;
        });

        // Display empty list view when having no results
        this.adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if (adapter.isEmpty()) {
                    // Display help text when no results available
                    listContainer.setVisibility(View.GONE);
                    emptyListView.setVisibility(View.VISIBLE);
                } else {
                    // Otherwise, display results
                    listContainer.setVisibility(View.VISIBLE);
                    emptyListView.setVisibility(View.GONE);
                }

                forwarderManager.onDataSetChanged();

            }
        });

        // Listen to changes
        searchEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Auto left-trim text.
                if (s.length() > 0 && s.charAt(0) == ' ')
                    s.delete(0, 1);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isViewingAllApps()) {
                    displayKissBar(false, false);
                }
                String text = s.toString();
                updateSearchRecords(text);
                displayClearOnInput();
            }
        });


        // Fixes bug when dropping onto a textEdit widget which can cause a NPE
        // This fix should be on ALL TextEdit Widgets !!!
        // See : https://stackoverflow.com/a/23483957
        searchEditText.setOnDragListener((v, event) -> true);


        // On validate, launch first record
        searchEditText.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == android.R.id.closeButton) {
                    systemUiVisibilityHelper.onKeyboardVisibilityChanged(false);
                    if (mPopup != null) {
                        mPopup.dismiss();
                        return true;
                    }
                    systemUiVisibilityHelper.onKeyboardVisibilityChanged(false);
                    hider.fixScroll();
                    return false;
                }
                RecordAdapter adapter = ((RecordAdapter) list.getAdapter());

                adapter.onClick(adapter.getCount() - 1, v);

                return true;
            }
        });

        registerForContextMenu(menuButton);

        // When scrolling down on the list,
        // Hide the keyboard.
        this.hider = new KeyboardScrollHider(this,
                this.list,
                (BottomPullEffectView) this.findViewById(R.id.listEdgeEffect)
        );
        this.hider.start();

        // Enable/disable phone/sms broadcast receiver
        PackageManagerUtils.enableComponent(this, IncomingSmsHandler.class, prefs.getBoolean("enable-sms-history", false));
        PackageManagerUtils.enableComponent(this, IncomingCallHandler.class, prefs.getBoolean("enable-phone-history", false));

        // Hide the "X" after the text field, instead displaying the menu button
        displayClearOnInput();

        systemUiVisibilityHelper = new SystemUiVisibilityHelper(this);

        /*
         * Defer everything else to the forwarders
         */
        forwarderManager.onCreate();

        //Appodeal
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(this, appKey, Appodeal.BANNER);
        Appodeal.show(this, Appodeal.BANNER_TOP);

        if(isOnline(MainActivity.this)) {
//            Toast.makeText(MainActivity.this, "Internet Connection",
//                    Toast.LENGTH_LONG).show();
            onResume();
        }

        Appodeal.setBannerCallbacks(new BannerCallbacks() {
            @Override
            public void onBannerLoaded(int height, boolean isPrecache) {
                Log.d("Appodeal", "onBannerLoaded");
            }
            @Override
            public void onBannerFailedToLoad() {
                Log.d("Appodeal", "onBannerFailedToLoad");

                if(isOnline(MainActivity.this))
                {
//                    Toast.makeText(MainActivity.this, "Internet Connection",
//                            Toast.LENGTH_LONG).show();

                    Appodeal.show(MainActivity.this, Appodeal.BANNER_TOP);
                    Appodeal.onResume(MainActivity.this, Appodeal.BANNER);

                    prefs.getBoolean("require-layout-update", false);
                    Log.i(TAG, "Restarting app after setting changes");
                    // Restart current activity to refresh view, since some preferences
                    // may require using a new UI
                    prefs.edit().putBoolean("require-layout-update", false).apply();
                    MainActivity.this.recreate();

                    return;

                }
/*
                else
                    {
                        Toast.makeText(MainActivity.this, "No Internet Connection",
                                Toast.LENGTH_LONG).show();
                        //return;
                    }
*/
            }
            @Override
            public void onBannerShown() {

                Log.d("Appodeal", "onBannerShown");

                String token = MainActivity.this.getUserToken();
                if (token != null) {
                    Api.addImpressionToUser addImpression = new Api.addImpressionToUser();
                    addImpression.delegate = MainActivity.this;
                    addImpression.execute(appKey, token);
                }
            }
            @Override
            public void onBannerClicked() {
                Log.d("Appodeal", "onBannerClicked");
            }
            @Override
            public void onBannerExpired() {
                Log.d("Appodeal", "onBannerExpired");
            }
        });

        avatar = this.findViewById(R.id.user_profile_pic);
        avatar.setOnClickListener((it -> MainActivity.this.launchNAULogin()));

        if (getFirstOpen() == null)
        {
            setFirstOpen();
            launchNAULogin();
        }

/*
        //Swipe Right for myNAU Feed
        ImageView background = findViewById(R.id.background_main);
        background.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
            public void onSwipeRight() {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), myNauFeedActivity.class);
                startActivity(intent);
            }

            public void onSwipeLeft() {
            }

        });
*/


        printhashkey();
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        forwarderManager.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        forwarderManager.onStart();
    }

    /**
     * Restart if required,
     * Hide the kissbar by default
     */
    @SuppressLint("CommitPrefEdits")
    protected void onResume() {
        Log.d(TAG, "onResume()");
        shouldRunOnResumeActions = true;

        if (prefs.getBoolean("require-layout-update", false)) {
            super.onResume();

            Log.i(TAG, "Restarting app after setting changes");
            // Restart current activity to refresh view, since some preferences
            // may require using a new UI
            prefs.edit().putBoolean("require-layout-update", false).apply();
            this.recreate();

            return;
        }

        dismissPopup();

        if (KissApplication.getApplication(this).getDataHandler().allProvidersHaveLoaded) {
            displayLoader(false);
            onFavoriteChange();
        }

        // We need to update the history in case an external event created new items
        // (for instance, installed a new app, got a phone call or simply clicked on a favorite)
        updateSearchRecords();
        displayClearOnInput();

        if (isViewingAllApps()) {
            displayKissBar(false);
        }

        forwarderManager.onResume();

        super.onResume();

        Appodeal.show(this, Appodeal.BANNER_TOP);
        Appodeal.onResume(this, Appodeal.BANNER);

        if (this.getUserToken() == null) {
/*
            if (appMenu != null) {
                Menu varMenu = appMenu;
//                if (this.appMenu == null) {
//                    Intrinsics.throwNpe();
//                }

                MenuItem varMenuItem = varMenu.findItem(R.id.action_login);
                Intrinsics.checkExpressionValueIsNotNull(varMenuItem, "appMenu!!.findItem(R.id.action_login)");
                loginBTN = varMenuItem;
                loginBTN.setVisible(true);

                loginBTN = varMenu.findItem(R.id.action_login);
                loginBTN.setVisible(true);
            }
*/

//            val logoutBTN: MenuItem = appMenu!!.findItem(R.id.action_logout)
//            logoutBTN.isVisible = false

            View varView = this.findViewById(R.id.user_name);
            Intrinsics.checkExpressionValueIsNotNull(varView, "findViewById(R.id.user_name)");
            username = (TextView) varView;
            username.setText((CharSequence)this.getString(R.string.login));
            username.setGravity(Gravity.CENTER | Gravity.BOTTOM);
            varView = this.findViewById(R.id.btn_coins_nau);
            Intrinsics.checkExpressionValueIsNotNull(varView, "findViewById(R.id.btn_coins_nau)");
            icon = (ImageView) varView;
            icon.setVisibility(View.GONE);
            varView = this.findViewById(R.id.user_profile_pic);
            Intrinsics.checkExpressionValueIsNotNull(varView, "findViewById(R.id.user_profile_pic)");
            avatar = (ImageView) varView;
            avatar.setImageResource(R.drawable.nau_icon);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(this.mReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        forwarderManager.onStop();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // This is called when the user press Home again while already browsing MainActivity
        // onResume() will be called right after, hiding the kissbar if any.
        // http://developer.android.com/reference/android/app/Activity.html#onNewIntent(android.content.Intent)
        // Animation can't happen in this method, since the activity is not resumed yet, so they'll happen in the onResume()
        // https://github.com/Neamar/LauncherNAU/issues/569
        if (!searchEditText.getText().toString().isEmpty()) {
            Log.i(TAG, "Clearing search field");
            searchEditText.setText("");
        }

        // Hide kissbar when coming back to launchernau2
        if (isViewingAllApps()) {
            displayKissBar(false);
        }

        // Close the backButton context menu
        closeContextMenu();
    }

    @Override
    public void onBackPressed() {
        if (mPopup != null) {
            mPopup.dismiss();
        } else if (isViewingAllApps()) {
            displayKissBar(false);
        } else {
            // If no kissmenu, empty the search bar
            // (this will trigger a new event if the search bar was already empty)
            // (which means pressing back in minimalistic mode with history displayed
            // will hide history again)
            searchEditText.setText("");
        }
        // No call to super.onBackPressed(), since this would quit the launcher.
    }

    @Override
    public boolean onKeyDown(int keycode, @NonNull KeyEvent e) {
        if (keycode == KeyEvent.KEYCODE_MENU) {
            // For devices with a physical menu button, we still want to display *our* contextual menu
            menuButton.showContextMenu();
            menuButton.performHapticFeedback(LONG_PRESS);
            return true;
        }

        return super.onKeyDown(keycode, e);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (forwarderManager.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                return true;
            case R.id.wallpaper:
                hideKeyboard();
                Intent intent = new Intent(Intent.ACTION_SET_WALLPAPER);
                startActivity(Intent.createChooser(intent, getString(R.string.menu_wallpaper)));
                return true;
            case R.id.preferences:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
/*
            case R.id.action_login :
                this.launchNAULogin();
                return true;
*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

/*
        String token = this.getUserToken();
        if (token != null && menu.findItem(R.id.action_login) != null) {
            MenuItem varMenuItem = menu.findItem(R.id.action_login);
            Intrinsics.checkExpressionValueIsNotNull(varMenuItem, "menu.findItem(R.id.action_login)");
            loginBTN  = varMenuItem;
            loginBTN.setVisible(false);
            // if (menu.findItem(R.id.action_logout) != null) {
            //     val logoutBTN: MenuItem = menu.findItem(R.id.action_logout)
            //     logoutBTN.isVisible = true
            // }
        }

        appMenu = menu;
*/

        return true;
    }

    /**
     * Display menu, on short or long press.
     *
     * @param menuButton "kebab" menu (3 dots)
     */
    public void onMenuButtonClicked(View menuButton) {
        // When the launchernau2 bar is displayed, the button can still be clicked in a few areas (due to favorite margin)
        // To fix this, we discard any click event occurring when the kissbar is displayed
        if (!isViewingSearchResults()) {
            return;
        }
        if (!forwarderManager.onMenuButtonClicked(this.menuButton)) {
            this.menuButton.showContextMenu();
            this.menuButton.performHapticFeedback(LONG_PRESS);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        forwarderManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        forwarderManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (forwarderManager.onTouch(view, event)) {
            return true;
        }

        if (view.getId() == searchEditText.getId()) {
            if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                searchEditText.performClick();
            }
        }
        return true;
    }


    /**
     * Clear text content when touching the cross button
     */
    @SuppressWarnings("UnusedParameters")
    public void onClearButtonClicked(View clearButton) {
        searchEditText.setText("");
    }

    /**
     * Display LauncherNAU menu
     */
    public void onLauncherButtonClicked(View launcherButton) {
        // Display or hide the launchernau2 bar, according to current view tag (showMenu / hideMenu).
        displayKissBar(launcherButton.getTag().equals("showMenu"));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mPopup != null && ev.getActionMasked() == MotionEvent.ACTION_DOWN) {
            dismissPopup();
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    private void displayClearOnInput() {
        if (searchEditText.getText().length() > 0) {
            clearButton.setVisibility(View.VISIBLE);
            menuButton.setVisibility(View.INVISIBLE);
            mynauButton.setVisibility(View.INVISIBLE);
        } else {
            clearButton.setVisibility(View.INVISIBLE);
            menuButton.setVisibility(View.VISIBLE);
            mynauButton.setVisibility(View.VISIBLE);
        }
    }

    public void displayLoader(Boolean display) {
        int animationDuration = getResources().getInteger(
                android.R.integer.config_longAnimTime);

        // Do not display animation if launcher button is already visible
        if (!display && launcherButton.getVisibility() == View.INVISIBLE) {
            launcherButton.setVisibility(View.VISIBLE);

            // Animate transition from loader to launch button
            launcherButton.setAlpha(0);
            launcherButton.animate()
                    .alpha(1f)
                    .setDuration(animationDuration)
                    .setListener(null);
            loaderSpinner.animate()
                    .alpha(0f)
                    .setDuration(animationDuration)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            loaderSpinner.setVisibility(View.GONE);
                            loaderSpinner.setAlpha(1);
                        }
                    });
        } else if (display) {
            launcherButton.setVisibility(View.INVISIBLE);
            loaderSpinner.setVisibility(View.VISIBLE);
        }
    }

    public void onFavoriteChange() {
        forwarderManager.onFavoriteChange();
    }

    private void displayKissBar(Boolean display) {
        this.displayKissBar(display, true);
    }

    private void displayKissBar(boolean display, boolean clearSearchText) {
        dismissPopup();
        // get the center for the clipping circle
        int cx = (launcherButton.getLeft() + launcherButton.getRight()) / 2;
        int cy = (launcherButton.getTop() + launcherButton.getBottom()) / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(kissBar.getWidth(), kissBar.getHeight());

        if (display) {
            // Display the app list
            if (searchEditText.getText().length() != 0) {
                searchEditText.setText("");
            }
            resetTask();

            // Needs to be done after setting the text content to empty
            isDisplayingKissBar = true;

            searchTask = new ApplicationsSearcher(MainActivity.this);
            searchTask.executeOnExecutor(Searcher.SEARCH_THREAD);

            // Reveal the bar
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int animationDuration = getResources().getInteger(
                        android.R.integer.config_shortAnimTime);

                Animator anim = ViewAnimationUtils.createCircularReveal(kissBar, cx, cy, 0, finalRadius);
                anim.setDuration(animationDuration);
                anim.start();
            }
            kissBar.setVisibility(View.VISIBLE);
            mynauButton.setVisibility(View.INVISIBLE);

            // Display the alphabet on the scrollbar (#926)
            list.setFastScrollEnabled(true);
        } else {
            isDisplayingKissBar = false;
            // Hide the bar
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int animationDuration = getResources().getInteger(
                        android.R.integer.config_shortAnimTime);

                try {
                    Animator anim = ViewAnimationUtils.createCircularReveal(kissBar, cx, cy, finalRadius, 0);
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            kissBar.setVisibility(View.GONE);
                            mynauButton.setVisibility(View.VISIBLE);
                            super.onAnimationEnd(animation);
                        }
                    });
                    anim.setDuration(animationDuration);
                    anim.start();
                } catch(IllegalStateException e) {
                    // If the view hasn't been laid out yet, we can't animate it
                    kissBar.setVisibility(View.GONE);
                    mynauButton.setVisibility(View.VISIBLE);
                }
            } else {
                // No animation before Lollipop
                kissBar.setVisibility(View.GONE);
                mynauButton.setVisibility(View.VISIBLE);
            }

            if (clearSearchText) {
                searchEditText.setText("");
            }

            // Do not display the alphabetical scrollbar (#926)
            // They only make sense when displaying apps alphabetically, not for searching
            list.setFastScrollEnabled(false);
        }

        forwarderManager.onDisplayKissBar(display);
    }

    public void updateSearchRecords() {
        updateSearchRecords(searchEditText.getText().toString());
    }

    /**
     * This function gets called on query changes.
     * It will ask all the providers for data
     * This function is not called for non search-related changes! Have a look at onDataSetChanged() if that's what you're looking for :)
     *
     * @param query the query on which to search
     */
    private void updateSearchRecords(String query) {
        resetTask();
        dismissPopup();

        forwarderManager.updateSearchRecords(query);

        if (query.isEmpty()) {
            systemUiVisibilityHelper.resetScroll();
        } else {
            runTask(new QuerySearcher(this, query));
        }
    }

    public void runTask(Searcher task) {
        resetTask();
        searchTask = task;
        searchTask.executeOnExecutor(Searcher.SEARCH_THREAD);
    }

    public void resetTask() {
        if (searchTask != null) {
            searchTask.cancel(true);
            searchTask = null;
        }
    }

    /**
     * Call this function when we're leaving the activity after clicking a search result
     * to clear the search list.
     * We can't use onPause(), since it may be called for a configuration change
     */
    @Override
    public void launchOccurred() {
        // We selected an item on the list,
        // now we can cleanup the filter:
        if (!searchEditText.getText().toString().isEmpty()) {
            searchEditText.setText("");
            displayClearOnInput();
            hideKeyboard();
        } else if (isViewingAllApps()) {
            displayKissBar(false);
        }
    }

    public void registerPopup(ListPopup popup) {
        if (mPopup == popup)
            return;
        dismissPopup();
        mPopup = popup;
        popup.setVisibilityHelper(systemUiVisibilityHelper);
        popup.setOnDismissListener(() -> MainActivity.this.mPopup = null);
        hider.fixScroll();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        systemUiVisibilityHelper.onWindowFocusChanged(hasFocus);
        forwarderManager.onWindowFocusChanged(hasFocus);

        if (hasFocus && shouldRunOnResumeActions) {
            shouldRunOnResumeActions = false;

            String token = this.getUserToken();

            if (token != null) {
                Api.getUserDashboard userDashboard = new Api.getUserDashboard();
                userDashboard.delegate = this;
                userDashboard.execute(token);
            }
        }
    }


    public void showKeyboard() {
        searchEditText.requestFocus();
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert mgr != null;
        mgr.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);

        systemUiVisibilityHelper.onKeyboardVisibilityChanged(true);
    }

    @Override
    public void hideKeyboard() {

        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            //noinspection ConstantConditions
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        systemUiVisibilityHelper.onKeyboardVisibilityChanged(false);
        dismissPopup();
    }

    @Override
    public void applyScrollSystemUi() {
        systemUiVisibilityHelper.applyScrollSystemUi();
    }

    /**
     * Check if history / search or app list is visible
     *
     * @return true of history, false on app list
     */
    public boolean isViewingSearchResults() {
        return !isDisplayingKissBar;
    }

    public boolean isViewingAllApps() {
        return isDisplayingKissBar;
    }

    @Override
    public void beforeListChange() {
        list.prepareChangeAnim();
    }

    @Override
    public void afterListChange() {
        list.animateChange();
    }

    public void dismissPopup() {
        if (mPopup != null)
            mPopup.dismiss();
    }

    public void showMatchingTags( String tag ) {
        runTask(new TagsSearcher(this, tag));

        clearButton.setVisibility(View.VISIBLE);
        menuButton.setVisibility(View.INVISIBLE);
    }

    public void showHistory() {
        runTask(new HistorySearcher(this));

        clearButton.setVisibility(View.VISIBLE);
        menuButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Functions for myNAU process, login and tokens
     */
//    private final void launchNAULogin()
    private void launchNAULogin() {
        Intent intent = new Intent(this, MainLoginActivity.class);
        Log.v("myNau", "Launch Login");
        Appodeal.hide(this, Appodeal.BANNER);
        this.startActivity(intent);
    }

    @Override
    public void processFinish(@Nullable JSONObject output, int resultCode) throws JSONException {
        if (resultCode == 401) {
            this.removeUserToken();
        }

        else {
/*            Menu varMenu = appMenu;
            if (appMenu == null) {
                Intrinsics.throwNpe();
            }

            if (varMenu.findItem(R.id.action_login) != null) {
                varMenu = appMenu;
                if (appMenu == null) {
                    Intrinsics.throwNpe();
                }

                MenuItem varMenuItem = varMenu.findItem(R.id.action_login);
                Intrinsics.checkExpressionValueIsNotNull(varMenuItem, "appMenu!!.findItem(R.id.action_login)");
                loginBTN = varMenuItem;

                loginBTN = varMenu.findItem(R.id.action_login);
                loginBTN.setVisible(false);
            }
*/

//            if (appMenu!!.findItem(R.id.action_logout) != null) {
//                val logoutBTN: MenuItem = appMenu!!.findItem(R.id.action_logout)
//                logoutBTN.isVisible = true
//            }

            String userPhoto = output != null ? output.getString("avatar") : null;
            if (userPhoto != null && !Intrinsics.areEqual(userPhoto, "")) {
                avatar = this.findViewById(R.id.user_profile_pic);
                //(new DownloadImageTask(avatar)).execute(userPhoto); doesn't download and put user photo
            }

            String coins = output != null ? output.getString("balance") : null;

            if (coins != null) {
                View varView = this.findViewById(R.id.user_name);
                Intrinsics.checkExpressionValueIsNotNull(varView, "findViewById(R.id.user_name)");
                username = (TextView) varView;
                username.setText((CharSequence) coins);
                username.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
                varView = this.findViewById(R.id.btn_coins_nau);
                Intrinsics.checkExpressionValueIsNotNull(varView, "findViewById(R.id.btn_coins_nau)");
                icon = (ImageView) varView;
                icon.setVisibility(View.VISIBLE);
            }
        }
    }

    private String getUserToken() {
//        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
//        return preferences.getString(R.string.token.toString(), null)
        return this.getSharedPreferences(this.getString(R.string.token), Context.MODE_PRIVATE)
                .getString(this.getString(R.string.token), null);
    }

    private String getFirstOpen(){
        return this.getSharedPreferences("first_open", Context.MODE_PRIVATE)
                .getString("first_open", null);
    }

    private void setFirstOpen() {
        SharedPreferences.Editor editor = this.getSharedPreferences("first_open", Context.MODE_PRIVATE).edit();
        editor.putString("first_open", "OK").apply(); //it was commit() instead of apply()
    }
//    private final void removeUserToken()
    private void removeUserToken() {
//        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
//        return preferences.getString(R.string.token.toString(), null)
        this.getSharedPreferences(this.getString(R.string.token), Context.MODE_PRIVATE)
                .edit().remove(this.getString(R.string.token)).apply(); //it was commit() instead of apply()

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            LoginManager.getInstance().logOut();
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleSignInClient varGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intrinsics.checkExpressionValueIsNotNull(varGoogleSignInClient, "GoogleSignIn.getClient(this, gso)");
        GoogleSignInClient mGoogleSignInClient = varGoogleSignInClient;
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            mGoogleSignInClient.signOut();
        }
    }
    public void printhashkey()
    {
        try
        {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(""+this.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature :packageInfo.signatures)
            {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.d("KeyHash", android.util.Base64.encodeToString(messageDigest.digest(), android.util.Base64.DEFAULT));
            }
        }
        catch (Exception e)
        {

        }
    }

}
