package nau.launcher.launchernau2.dataprovider;

import android.widget.Toast;

import nau.launcher.launchernau2.R;
import nau.launcher.launchernau2.loader.LoadShortcutsPojos;
import nau.launcher.launchernau2.normalizer.StringNormalizer;
import nau.launcher.launchernau2.pojo.Pojo;
import nau.launcher.launchernau2.pojo.ShortcutsPojo;
import nau.launcher.launchernau2.searcher.Searcher;
import nau.launcher.launchernau2.utils.FuzzyScore;

public class ShortcutsProvider extends Provider<ShortcutsPojo> {

    @Override
    public void reload() {
        super.reload();
        // If the user tries to add a new shortcut, but LauncherNAU isn't the default launcher
        // AND the services are not running (low memory), then we won't be able to
        // spawn a new service on Android 8.1+.

        try {
            this.initialize(new LoadShortcutsPojos(this));
        }
        catch(IllegalStateException e) {
            Toast.makeText(this, R.string.unable_to_initialize_shortcuts, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void requestResults(String query, Searcher searcher) {
        StringNormalizer.Result queryNormalized = StringNormalizer.normalizeWithResult(query, false);

        if (queryNormalized.codePoints.length == 0) {
            return;
        }

        FuzzyScore fuzzyScore = new FuzzyScore(queryNormalized.codePoints);
        FuzzyScore.MatchInfo matchInfo;
        boolean match;

        for (ShortcutsPojo pojo : pojos) {
            matchInfo = fuzzyScore.match(pojo.normalizedName.codePoints);
            match = matchInfo.match;
            pojo.relevance = matchInfo.score;

            // check relevance for tags
            if (pojo.normalizedTags != null) {
                matchInfo = fuzzyScore.match(pojo.normalizedTags.codePoints);
                if (matchInfo.match && (!match || matchInfo.score > pojo.relevance)) {
                    match = true;
                    pojo.relevance = matchInfo.score;
                }
            }

            if (match && !searcher.addResult(pojo)) {
                return;
            }
        }
    }

    public Pojo findByName(String name) {
        for (Pojo pojo : pojos) {
            if (pojo.getName().equals(name))
                return pojo;
        }
        return null;
    }
}
