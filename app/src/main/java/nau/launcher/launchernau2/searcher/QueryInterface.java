package nau.launcher.launchernau2.searcher;

import nau.launcher.launchernau2.ui.ListPopup;

public interface QueryInterface {
    void launchOccurred();

    void registerPopup(ListPopup popup);
}
