package nau.launcher.launchernau2.forwarder;

import android.content.SharedPreferences;

import nau.launcher.launchernau2.MainActivity;

abstract class Forwarder {
    final MainActivity mainActivity;
    final SharedPreferences prefs;

    Forwarder(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.prefs = mainActivity.prefs;
    }
}