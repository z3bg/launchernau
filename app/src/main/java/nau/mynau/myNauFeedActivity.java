package nau.mynau;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import nau.launcher.launchernau2.MainActivity;
import nau.launcher.launchernau2.R;

public class myNauFeedActivity extends AppCompatActivity{

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mynau_feed);

        //Swipe Left for LauncherNAU
        ImageView backgroundNauFeed = findViewById(R.id.backgroundFeed);
        backgroundNauFeed.setOnTouchListener(new OnSwipeTouchListener(myNauFeedActivity.this) {
            public void onSwipeRight() {
            }

            public void onSwipeLeft() {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }

        });

    }
}
